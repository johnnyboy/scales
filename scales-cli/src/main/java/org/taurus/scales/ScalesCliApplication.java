package org.taurus.scales;

import org.jline.reader.History;
import org.jline.reader.impl.history.DefaultHistory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.util.StringUtils;

@SpringBootApplication
public class ScalesCliApplication {

	public static void main(String[] args) {
		String[] disabledCommands = {"--spring.shell.command.script.enabled=false", "--spring.shell.command.stacktrace.enabled=false"};
		String[] fullArgs = StringUtils.concatenateStringArrays(args, disabledCommands);
		SpringApplication.run(ScalesCliApplication.class, fullArgs);
	}

	@Bean
	public History nonPersistentShellHistory() {
		return new DefaultHistory() {
			@Override
			public void save() {
				//no-op
			}
		};
	}
}
