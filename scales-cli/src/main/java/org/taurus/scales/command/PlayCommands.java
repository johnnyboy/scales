package org.taurus.scales.command;

import org.springframework.shell.Availability;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellMethodAvailability;
import org.springframework.shell.standard.ShellOption;
import org.taurus.scales.autocompletion.ChordTypeValueProvider;
import org.taurus.scales.autocompletion.NoteValueProvider;
import org.taurus.scales.autocompletion.ScaleTypeValueProvider;
import org.taurus.scales.core.Chord;
import org.taurus.scales.core.Note;
import org.taurus.scales.core.Scale;
import org.taurus.scales.core.chords.Chords;
import org.taurus.scales.core.scales.DiatonicScale;
import org.taurus.scales.core.scales.PentatonicScale;
import org.taurus.scales.core.scales.Scales;
import org.taurus.scales.midi.Player;

@ShellComponent
public class PlayCommands {

	private final Player player;

	public PlayCommands(Player player) {
		this.player = player;
	}

	@ShellMethod("plays a chord of given type and note")
	public void playChord(@ShellOption(value = "--of", valueProvider = NoteValueProvider.class) Note note, @ShellOption(value = "--type", valueProvider = ChordTypeValueProvider.class) Chords chordType) {
		Chord chord = chordType.of(note);
		player.play(chord);
	}

	@ShellMethod("plays a scale of given type and note")
	public void playScale(@ShellOption(value = "--of", valueProvider = NoteValueProvider.class) Note note, @ShellOption(value = "--type", valueProvider = ScaleTypeValueProvider.class) Scales scaleType) {
		Scale scale = scaleType.of(note);
		player.play(scale);
	}

	@ShellMethod("plays chord progressions of a given scale type and note")
	public void playScaleChords(@ShellOption(value = "--of", valueProvider = NoteValueProvider.class) Note note, @ShellOption(value = "--type", valueProvider = ScaleTypeValueProvider.class) Scales scaleType) {
		Scale scale = scaleType.of(note);
		if (scale instanceof DiatonicScale) {
			player.play(((DiatonicScale) scale).chordProgressions());
		} else if (scale instanceof PentatonicScale) {
			player.play(((PentatonicScale) scale).chordProgressions());
		}
	}

	@ShellMethodAvailability
	public Availability playerAvailability() {
		return player.isAvailable() ? Availability.available() : Availability.unavailable("your system does not support MIDI sequencer");
	}
}
