package org.taurus.scales.command;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import org.taurus.scales.autocompletion.ChordTypeValueProvider;
import org.taurus.scales.autocompletion.NoteValueProvider;
import org.taurus.scales.core.Chord;
import org.taurus.scales.core.Note;
import org.taurus.scales.core.chords.Chords;
import org.taurus.scales.registry.TypeRegistry;

import java.util.stream.Collectors;
import java.util.stream.Stream;

@ShellComponent
public class ChordCommands {

	private final TypeRegistry typeRegistry;

	public ChordCommands(TypeRegistry typeRegistry) {
		this.typeRegistry = typeRegistry;
	}

	@ShellMethod("prints all chords of a given note")
	public String chords(@ShellOption(value = "--of", valueProvider = NoteValueProvider.class) Note note) {
		return Stream.of(note.chord(Chords.MAJOR),
				note.chord(Chords.MINOR),
				note.chord(Chords.AUGMENTED),
				note.chord(Chords.DIMINISHED),
				note.chord(Chords.MAJOR_SEVENTH),
				note.chord(Chords.MINOR_SEVENTH),
				note.chord(Chords.DOMINANT_SEVENTH),
				note.chord(Chords.SUS2),
				note.chord(Chords.SUS4))
				.map(Chord::toString).collect(Collectors.joining(System.lineSeparator()));
	}

	@ShellMethod("prints chord of given type and note")
	public String chord(@ShellOption(value = "--of", valueProvider = NoteValueProvider.class) Note note, @ShellOption(value = "--type", valueProvider = ChordTypeValueProvider.class) Chords chordType) {
		Chord chord = chordType.of(note);
		return chord.toString();
	}

	@ShellMethod("lists valid chord types")
	public String chordTypes() {
		return typeRegistry.chordTypes().toString();
	}
}
