package org.taurus.scales.command;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import org.taurus.scales.autocompletion.NoteValueProvider;
import org.taurus.scales.autocompletion.ScaleTypeValueProvider;
import org.taurus.scales.core.Chord;
import org.taurus.scales.core.Note;
import org.taurus.scales.core.Scale;
import org.taurus.scales.core.scales.DiatonicScale;
import org.taurus.scales.core.scales.PentatonicScale;
import org.taurus.scales.core.scales.Scales;
import org.taurus.scales.registry.TypeRegistry;

import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ShellComponent
public class ScaleCommands {

	private final TypeRegistry typeRegistry;

	public ScaleCommands(TypeRegistry typeRegistry) {
		this.typeRegistry = typeRegistry;
	}

	@ShellMethod("prints all scales of given note")
	public String scales(@ShellOption(value = "--of", valueProvider = NoteValueProvider.class) Note note) {
		return Stream.of(note.scale(Scales.MAJOR),
				note.scale(Scales.DORIAN),
				note.scale(Scales.PHRYGIAN),
				note.scale(Scales.LYDIAN),
				note.scale(Scales.MIXOLYDIAN),
				note.scale(Scales.MINOR),
				note.scale(Scales.LOCRIAN),
				note.scale(Scales.HARMONIC_MINOR),
				note.scale(Scales.MELODIC_MINOR),
				note.scale(Scales.MAJOR_PENTATONIC),
				note.scale(Scales.MINOR_PENTATONIC),
				note.scale(Scales.BLUES)
		).map(this::format).collect(Collectors.joining(System.lineSeparator()));
	}

	private String format(Scale scale) {
		return String.format("%-40s%s", scale.name(), scale.list());
	}

	@ShellMethod("prints detailed scale information by scale type and note")
	public String scale(@ShellOption(value = "--of", valueProvider = NoteValueProvider.class) Note note, @ShellOption(value = "--type", valueProvider = ScaleTypeValueProvider.class) Scales scaleType) {
		StringJoiner joiner = new StringJoiner(System.lineSeparator());
		Scale scale = scaleType.of(note);
		if (scale instanceof DiatonicScale) {
			DiatonicScale diatonicScale = (DiatonicScale) scale;
			joiner.add(format(scale));
			joiner.add("Relative -- ");
			joiner.add(format(diatonicScale.relative()));
			joiner.add("Parallel -- ");
			joiner.add(format(diatonicScale.parallel()));
			joiner.add("Chords -- ");
			for (Chord chord : diatonicScale.chords()) {
				joiner.add(chord.toString());
			}
			return joiner.toString();
		}
		if (scale instanceof PentatonicScale) {
			PentatonicScale pentatonicScale = (PentatonicScale) scale;
			joiner.add(format(scale));
			joiner.add("Relative -- ");
			joiner.add(format(pentatonicScale.relative()));
			joiner.add("Parallel -- ");
			joiner.add(format(pentatonicScale.parallel()));
			joiner.add("Chords -- ");
			for (Chord chord : pentatonicScale.chords()) {
				joiner.add(chord.toString());
			}
			return joiner.toString();
		}
		return scale.toString();
	}

	@ShellMethod("lists valid scale types")
	public String scaleTypes() {
		return typeRegistry.scaleTypes().toString();
	}
}
