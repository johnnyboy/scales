package org.taurus.scales.midi;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;
import org.taurus.scales.core.Chord;
import org.taurus.scales.core.Note;
import org.taurus.scales.core.Scale;
import org.taurus.scales.core.chords.Chords;
import org.taurus.scales.core.scales.ChordProgression;
import org.taurus.scales.core.scales.Scales;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class Player implements InitializingBean {
	private static final int CHANNEL = 1;

	private boolean available;

	@Override
	public void afterPropertiesSet() {
		try {
			Sequencer sequencer = MidiSystem.getSequencer();
			sequencer.open();
			sequencer.close();
			available = true;
			play(Chords.MINOR.of(Note.C));
			play(Scales.MINOR.of(Note.C));
		} catch (MidiUnavailableException e) {
			available = false;
		}
	}

	private Sequence sequence(Scale scale) {
		try {
			Sequence sequence = new Sequence(Sequence.PPQ, 4);

			Track track = sequence.createTrack();
			track.add(defaultInstrument());
			System.out.println("Playing scale " + scale);
			List<Note> notes = scale.list();
			Note root = notes.get(0);
			Note last = notes.get(notes.size() - 1);
			boolean cyclicScale = root.equals(last);
			if (cyclicScale) {
				for (int i = 0; i < notes.size() - 1; i++) {
					Note note = notes.get(i);
					track.add(noteOn(noteNumber(note, root), track.ticks()));
					track.add(noteOff(noteNumber(note, root), track.ticks() + 7));
				}
				track.add(noteOn(octaveUp(last), track.ticks()));
				track.add(noteOff(octaveUp(last), track.ticks() + 7));
			} else {
				for (Note note : notes) {
					track.add(noteOn(noteNumber(note, root), track.ticks()));
					track.add(noteOff(noteNumber(note, root), track.ticks() + 7));
				}
			}
			return sequence;
		} catch (InvalidMidiDataException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	private Sequence sequence(Chord chord) {
		try {
			Sequence sequence = new Sequence(Sequence.PPQ, 4);

			Track track = sequence.createTrack();
			track.add(defaultInstrument());

			System.out.println("Playing chord " + chord);
			List<Note> notes = chord.list();
			for (Note note : notes) {
				track.add(noteOn(noteNumber(note, notes.get(0)), 1));
				track.add(noteOff(noteNumber(note, notes.get(0)), 24));
			}
			return sequence;
		} catch (InvalidMidiDataException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	private Sequence sequence(Collection<ChordProgression> chordProgressions) {
		try {
			Sequence sequence = new Sequence(Sequence.PPQ, 4);

			Track track = sequence.createTrack();
			track.add(defaultInstrument());

			for (ChordProgression chordProgression : chordProgressions) {
				System.out.printf("Playing chord progression %s%n", chordProgression.chords().stream().map(Chord::shortName).collect(Collectors.joining("|")));
				for (Chord chord : chordProgression.chords()) {
					final long ticks = track.ticks();
					for (Note note : chord.list()) {
						track.add(noteOn(noteNumber(note, chord.list().get(0)), ticks));
						track.add(noteOff(noteNumber(note, chord.list().get(0)), ticks + 24));
					}
				}
			}
			return sequence;
		} catch (InvalidMidiDataException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	private MidiEvent noteOn(int note, long tick) throws InvalidMidiDataException {
		final int velocity = 100;
		ShortMessage msg = new ShortMessage(ShortMessage.NOTE_ON, CHANNEL, note, velocity);
		return new MidiEvent(msg, tick);
	}

	private MidiEvent noteOff(int note, long tick) throws InvalidMidiDataException {
		final int fadeVelocity = 50;
		ShortMessage msg = new ShortMessage(ShortMessage.NOTE_OFF, CHANNEL, note, fadeVelocity);
		return new MidiEvent(msg, tick);
	}

	private int noteNumber(Note note, Note root) {
		if (root.ordinal() <= note.ordinal()) {
			return baseOctave(note);
		}
		return octaveUp(note);
	}

	private int baseOctave(Note note) {
		return 48 + note.ordinal();
	}

	private int octaveUp(Note note) {
		return 60 + note.ordinal();
	}

	private MidiEvent defaultInstrument() throws InvalidMidiDataException {
		ShortMessage msg = new ShortMessage(ShortMessage.PROGRAM_CHANGE, CHANNEL, 0, 0);
		return new MidiEvent(msg, 1);
	}

	public boolean isAvailable() {
		return available;
	}

	public void play(Chord chord) {
		play(sequence(chord));
	}

	public void play(Collection<ChordProgression> chordProgressions) {
		play(sequence(chordProgressions));
	}

	public void play(Scale scale) {
		play(sequence(scale));
	}

	private void play(Sequence sequence) {
		try {
			final Sequencer sequencer = MidiSystem.getSequencer();
			sequencer.open();
			sequencer.setSequence(sequence);
			sequencer.start();
			sequencer.addMetaEventListener(meta -> {
				final int endOfTrack = 0x2F;
				if (meta.getType() == endOfTrack) {
					sequencer.close();
				}
			});
		} catch (InvalidMidiDataException e) {
			throw new RuntimeException(e.getMessage(), e);
		} catch (MidiUnavailableException e) {
			available = false;
			throw new RuntimeException(e.getMessage(), e);
		}
	}
}
