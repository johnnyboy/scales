package org.taurus.scales.autocompletion;

import org.springframework.stereotype.Component;
import org.taurus.scales.core.scales.Scales;
import org.taurus.scales.registry.TypeRegistry;

import java.util.Collection;

@Component
public class ScaleTypeValueProvider extends AbstractValueProvider {

	protected ScaleTypeValueProvider(TypeRegistry typeRegistry) {
		super(typeRegistry);
	}

	@Override
	protected Collection<String> values() {
		return typeRegistry.scaleTypes();
	}

	@Override
	protected Class<?> target() {
		return Scales.class;
	}
}
