package org.taurus.scales.autocompletion;

import org.springframework.stereotype.Component;
import org.taurus.scales.core.Note;
import org.taurus.scales.registry.TypeRegistry;

import java.util.Collection;

@Component
public class NoteValueProvider extends AbstractValueProvider {

	protected NoteValueProvider(TypeRegistry typeRegistry) {
		super(typeRegistry);
	}

	@Override
	protected Collection<String> values() {
		return typeRegistry.notes();
	}

	@Override
	protected Class<?> target() {
		return Note.class;
	}
}
