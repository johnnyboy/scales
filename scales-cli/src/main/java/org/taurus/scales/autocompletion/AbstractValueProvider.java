package org.taurus.scales.autocompletion;

import org.springframework.core.MethodParameter;
import org.springframework.shell.CompletionContext;
import org.springframework.shell.CompletionProposal;
import org.springframework.shell.standard.ValueProvider;
import org.springframework.stereotype.Component;
import org.taurus.scales.registry.TypeRegistry;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public abstract class AbstractValueProvider implements ValueProvider {
	final TypeRegistry typeRegistry;

	protected AbstractValueProvider(TypeRegistry typeRegistry) {
		this.typeRegistry = typeRegistry;
	}

	@Override
	public boolean supports(MethodParameter parameter, CompletionContext completionContext) {
		return parameter.getParameterType().isAssignableFrom(target());
	}

	@Override
	public List<CompletionProposal> complete(MethodParameter parameter, CompletionContext completionContext, String[] hints) {
		return values().stream()
				.filter(s -> s.contains(completionContext.currentWordUpToCursor()))
				.map(CompletionProposal::new)
				.collect(Collectors.toList());
	}

	protected abstract Collection<String> values();

	protected abstract Class<?> target();
}
