package org.taurus.scales.autocompletion;

import org.springframework.stereotype.Component;
import org.taurus.scales.core.chords.Chords;
import org.taurus.scales.registry.TypeRegistry;

import java.util.Collection;

@Component
public class ChordTypeValueProvider extends AbstractValueProvider {

	protected ChordTypeValueProvider(TypeRegistry typeRegistry) {
		super(typeRegistry);
	}

	@Override
	protected Collection<String> values() {
		return typeRegistry.chordTypes();
	}

	@Override
	protected Class<?> target() {
		return Chords.class;
	}
}
