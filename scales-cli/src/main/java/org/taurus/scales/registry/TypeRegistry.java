package org.taurus.scales.registry;

import org.springframework.stereotype.Component;
import org.taurus.scales.core.Note;
import org.taurus.scales.core.chords.Chords;
import org.taurus.scales.core.scales.Scales;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class TypeRegistry {
	private final Collection<String> scales;
	private final Collection<String> chords;
	private final Collection<String> notes;

	public TypeRegistry() {
		scales = Collections.unmodifiableCollection(Stream.of(Scales.values())
				.map(Enum::name).map(this::enumNameToKey)
				.collect(Collectors.toList()));
		chords = Collections.unmodifiableCollection(Stream.of(Chords.values())
				.map(Enum::name).map(this::enumNameToKey)
				.collect(Collectors.toList()));
		notes = Collections.unmodifiableCollection(Stream.of(Note.values())
				.map(n -> n.name).collect(Collectors.toList()));
	}

	private String enumNameToKey(String enumName) {
		return enumName.toLowerCase().replace('_', '-');
	}

	private String keyToEnumName(String key) {
		return key.toUpperCase().replace('-', '_');
	}

	public Collection<String> scaleTypes() {
		return scales;
	}

	public Collection<String> chordTypes() {
		return chords;
	}

	public Collection<String> notes() {
		return notes;
	}

	public Optional<Scales> scaleType(String key) {
		return Stream.of(Scales.values())
				.filter(s -> s.name().equals(keyToEnumName(key)))
				.findFirst();
	}

	public Optional<Chords> chordType(String key) {
		return Stream.of(Chords.values())
				.filter(c -> c.name().equals(keyToEnumName(key)))
				.findFirst();
	}

	public Optional<Note> note(String key) {
		try {
			return Optional.of(Note.of(key));
		} catch (IllegalArgumentException e) {
			return Optional.empty();
		}
	}


}
