package org.taurus.scales.converter;

import org.springframework.stereotype.Component;
import org.taurus.scales.core.scales.Scales;
import org.taurus.scales.registry.TypeRegistry;

import java.util.Collection;
import java.util.Optional;

@Component
public class ScaleTypeConverter extends AbstractConverter<Scales> {

	protected ScaleTypeConverter(TypeRegistry typeRegistry) {
		super(typeRegistry);
	}

	@Override
	protected Optional<Scales> get(String source) {
		return typeRegistry.scaleType(source);
	}

	@Override
	protected Collection<String> values() {
		return typeRegistry.scaleTypes();
	}
}
