package org.taurus.scales.converter;

import org.springframework.stereotype.Component;
import org.taurus.scales.core.Note;
import org.taurus.scales.registry.TypeRegistry;

import java.util.Collection;
import java.util.Optional;

@Component
public class NoteConverter extends AbstractConverter<Note> {

	protected NoteConverter(TypeRegistry typeRegistry) {
		super(typeRegistry);
	}

	@Override
	protected Optional<Note> get(String source) {
		return typeRegistry.note(source);
	}

	@Override
	protected Collection<String> values() {
		return typeRegistry.notes();
	}
}
