package org.taurus.scales.converter;

import org.springframework.stereotype.Component;
import org.taurus.scales.core.chords.Chords;
import org.taurus.scales.registry.TypeRegistry;

import java.util.Collection;
import java.util.Optional;

@Component
public class ChordTypeConverter extends AbstractConverter<Chords> {

	protected ChordTypeConverter(TypeRegistry typeRegistry) {
		super(typeRegistry);
	}

	@Override
	protected Optional<Chords> get(String source) {
		return typeRegistry.chordType(source);
	}

	@Override
	protected Collection<String> values() {
		return typeRegistry.chordTypes();
	}
}
