package org.taurus.scales.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.taurus.scales.registry.TypeRegistry;

import java.util.Collection;
import java.util.Optional;

@Component
public abstract class AbstractConverter<T> implements Converter<String, T> {

	protected final TypeRegistry typeRegistry;

	protected AbstractConverter(TypeRegistry typeRegistry) {
		this.typeRegistry = typeRegistry;
	}

	protected abstract Optional<T> get(String source);

	protected abstract Collection<String> values();

	@Override
	public T convert(String source) {
		return get(source).orElseThrow(() -> new IllegalArgumentException(String.format("No match for %s, try one of %s", source, values())));
	}
}
