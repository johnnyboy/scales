### scales [![Run Status](https://api.shippable.com/projects/5b44e411a3a4cd07008bf1fa/badge?branch=master)]()  
a multi-module java project, scratching the surface of music theory, scales & chords

#### scales-core [![Coverage Badge](https://api.shippable.com/projects/5b44e411a3a4cd07008bf1fa/coverageBadge?branch=master)]()  
core library to work with notes, scales and chords

##### Supported scales
* natural major (ionian mode)
* dorian mode
* phrygian mode
* lydian mode
* mixolydian mode
* natural minor (aeolian mode)
* locrian mode
* harmonic minor
* melodic minor
* major pentatonic
* minor pentatonic
* blues hexatonic

##### Supported chords
* major triad
* minor triad
* diminished
* augmented
* suspended second
* suspended fourth
* major seventh
* minor seventh
* dominant seventh

#### scales-cli
command-line client with a basic MIDI integration, dependent on core

##### Features
* scale playback
* chord playback
* terse and readable commands `play-chord A minor`
* smart `TAB` completion of commands and arguments


##### Libraries used:
* [Spring shell - CLI on top of Spring Boot](https://projects.spring.io/spring-shell/)
* [JUnit4 - test framework](https://junit.org/junit4/)
* [JUnitParams - parameterized tests](https://github.com/Pragmatists/JUnitParams)
* [AssertJ - fluent assertions](http://joel-costigliola.github.io/assertj/)

##### Build
`mvn clean package` - creates an executable jar under `/scales-cli/target/`   
code coverage report by [JaCoCo](https://www.eclemma.org/jacoco/)  
CI support by [Shippable](http://www.shippable.com/)

##### Run
`java -jar scales-cli/target/scales-cli.jar`  
type `help` to get a list of supported commands
