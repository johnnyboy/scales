package org.taurus.scales.core.chords;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.taurus.scales.core.Chord;
import org.taurus.scales.core.Note;
import org.taurus.scales.core.chords.Chords;
import org.taurus.scales.core.chords.SeventhChord;
import org.taurus.scales.core.chords.SuspendedChord;
import org.taurus.scales.core.chords.TriadChord;
import org.taurus.scales.core.scales.DiatonicScale;
import org.taurus.scales.core.scales.Scales;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.taurus.scales.core.TestUtils.randomNote;

@RunWith(JUnitParamsRunner.class)
public class ChordTest {

	@Test
	@Parameters(method = "chordCorrectnessParameters")
	public void chordCorrectness(Chords chordType, Note root, String chord) {
		//given
		String[] expected = chord.split(" ");

		//when
		String[] actual = chordType.of(root).list().stream().map(String::valueOf).toArray(String[]::new);

		//then
		assertThat(actual).isEqualTo(expected);
	}

	@SuppressWarnings("unused")
	Object[] chordCorrectnessParameters() {
		return new Object[]{
				new Object[]{Chords.MAJOR, Note.C, "C E G"},
				new Object[]{Chords.MAJOR, Note.C_SHARP, "C# F Ab"},
				new Object[]{Chords.MAJOR, Note.D, "D F# A"},
				new Object[]{Chords.MAJOR, Note.E_FLAT, "Eb G Bb"},
				new Object[]{Chords.MAJOR, Note.E, "E Ab B"},
				new Object[]{Chords.MAJOR, Note.F, "F A C"},
				new Object[]{Chords.MAJOR, Note.F_SHARP, "F# Bb C#"},
				new Object[]{Chords.MAJOR, Note.G, "G B D"},
				new Object[]{Chords.MAJOR, Note.A_FLAT, "Ab C Eb"},
				new Object[]{Chords.MAJOR, Note.A, "A C# E"},
				new Object[]{Chords.MAJOR, Note.B_FLAT, "Bb D F"},
				new Object[]{Chords.MAJOR, Note.B, "B Eb F#"},
				new Object[]{Chords.DIMINISHED, Note.C, "C Eb F#"},
				new Object[]{Chords.DIMINISHED, Note.C_SHARP, "C# E G"},
				new Object[]{Chords.DIMINISHED, Note.D, "D F Ab"},
				new Object[]{Chords.DIMINISHED, Note.E_FLAT, "Eb F# A"},
				new Object[]{Chords.DIMINISHED, Note.E, "E G Bb"},
				new Object[]{Chords.DIMINISHED, Note.F, "F Ab B"},
				new Object[]{Chords.DIMINISHED, Note.F_SHARP, "F# A C"},
				new Object[]{Chords.DIMINISHED, Note.G, "G Bb C#"},
				new Object[]{Chords.DIMINISHED, Note.A_FLAT, "Ab B D"},
				new Object[]{Chords.DIMINISHED, Note.A, "A C Eb"},
				new Object[]{Chords.DIMINISHED, Note.B_FLAT, "Bb C# E"},
				new Object[]{Chords.DIMINISHED, Note.B, "B D F"},
				new Object[]{Chords.MINOR, Note.C, "C Eb G"},
				new Object[]{Chords.MINOR, Note.C_SHARP, "C# E Ab"},
				new Object[]{Chords.MINOR, Note.D, "D F A"},
				new Object[]{Chords.MINOR, Note.E_FLAT, "Eb F# Bb"},
				new Object[]{Chords.MINOR, Note.E, "E G B"},
				new Object[]{Chords.MINOR, Note.F, "F Ab C"},
				new Object[]{Chords.MINOR, Note.F_SHARP, "F# A C#"},
				new Object[]{Chords.MINOR, Note.G, "G Bb D"},
				new Object[]{Chords.MINOR, Note.A_FLAT, "Ab B Eb"},
				new Object[]{Chords.MINOR, Note.A, "A C E"},
				new Object[]{Chords.MINOR, Note.B_FLAT, "Bb C# F"},
				new Object[]{Chords.MINOR, Note.B, "B D F#"},
				new Object[]{Chords.AUGMENTED, Note.C, "C E Ab"},
				new Object[]{Chords.AUGMENTED, Note.C_SHARP, "C# F A"},
				new Object[]{Chords.AUGMENTED, Note.D, "D F# Bb"},
				new Object[]{Chords.AUGMENTED, Note.E_FLAT, "Eb G B"},
				new Object[]{Chords.AUGMENTED, Note.E, "E Ab C"},
				new Object[]{Chords.AUGMENTED, Note.F, "F A C#"},
				new Object[]{Chords.AUGMENTED, Note.F_SHARP, "F# Bb D"},
				new Object[]{Chords.AUGMENTED, Note.G, "G B Eb"},
				new Object[]{Chords.AUGMENTED, Note.A_FLAT, "Ab C E"},
				new Object[]{Chords.AUGMENTED, Note.A, "A C# F"},
				new Object[]{Chords.AUGMENTED, Note.B_FLAT, "Bb D F#"},
				new Object[]{Chords.AUGMENTED, Note.B, "B Eb G"},
				new Object[]{Chords.SUS2, Note.C, "C D G"},
				new Object[]{Chords.SUS2, Note.C_SHARP, "C# Eb Ab"},
				new Object[]{Chords.SUS2, Note.D, "D E A"},
				new Object[]{Chords.SUS2, Note.E_FLAT, "Eb F Bb"},
				new Object[]{Chords.SUS2, Note.E, "E F# B"},
				new Object[]{Chords.SUS2, Note.F, "F G C"},
				new Object[]{Chords.SUS2, Note.F_SHARP, "F# Ab C#"},
				new Object[]{Chords.SUS2, Note.G, "G A D"},
				new Object[]{Chords.SUS2, Note.A_FLAT, "Ab Bb Eb"},
				new Object[]{Chords.SUS2, Note.A, "A B E"},
				new Object[]{Chords.SUS2, Note.B_FLAT, "Bb C F"},
				new Object[]{Chords.SUS2, Note.B, "B C# F#"},
				new Object[]{Chords.SUS4, Note.C, "C F G"},
				new Object[]{Chords.SUS4, Note.C_SHARP, "C# F# Ab"},
				new Object[]{Chords.SUS4, Note.D, "D G A"},
				new Object[]{Chords.SUS4, Note.E_FLAT, "Eb Ab Bb"},
				new Object[]{Chords.SUS4, Note.E, "E A B"},
				new Object[]{Chords.SUS4, Note.F, "F Bb C"},
				new Object[]{Chords.SUS4, Note.F_SHARP, "F# B C#"},
				new Object[]{Chords.SUS4, Note.G, "G C D"},
				new Object[]{Chords.SUS4, Note.A_FLAT, "Ab C# Eb"},
				new Object[]{Chords.SUS4, Note.A, "A D E"},
				new Object[]{Chords.SUS4, Note.B_FLAT, "Bb Eb F"},
				new Object[]{Chords.SUS4, Note.B, "B E F#"},
				new Object[]{Chords.MAJOR_SEVENTH, Note.C, "C E G B"},
				new Object[]{Chords.MAJOR_SEVENTH, Note.C_SHARP, "C# F Ab C"},
				new Object[]{Chords.MAJOR_SEVENTH, Note.D, "D F# A C#"},
				new Object[]{Chords.MAJOR_SEVENTH, Note.E_FLAT, "Eb G Bb D"},
				new Object[]{Chords.MAJOR_SEVENTH, Note.E, "E Ab B Eb"},
				new Object[]{Chords.MAJOR_SEVENTH, Note.F, "F A C E"},
				new Object[]{Chords.MAJOR_SEVENTH, Note.F_SHARP, "F# Bb C# F"},
				new Object[]{Chords.MAJOR_SEVENTH, Note.G, "G B D F#"},
				new Object[]{Chords.MAJOR_SEVENTH, Note.A_FLAT, "Ab C Eb G"},
				new Object[]{Chords.MAJOR_SEVENTH, Note.A, "A C# E Ab"},
				new Object[]{Chords.MAJOR_SEVENTH, Note.B_FLAT, "Bb D F A"},
				new Object[]{Chords.MAJOR_SEVENTH, Note.B, "B Eb F# Bb"},
				new Object[]{Chords.DOMINANT_SEVENTH, Note.C, "C E G Bb"},
				new Object[]{Chords.DOMINANT_SEVENTH, Note.C_SHARP, "C# F Ab B"},
				new Object[]{Chords.DOMINANT_SEVENTH, Note.D, "D F# A C"},
				new Object[]{Chords.DOMINANT_SEVENTH, Note.E_FLAT, "Eb G Bb C#"},
				new Object[]{Chords.DOMINANT_SEVENTH, Note.E, "E Ab B D"},
				new Object[]{Chords.DOMINANT_SEVENTH, Note.F, "F A C Eb"},
				new Object[]{Chords.DOMINANT_SEVENTH, Note.F_SHARP, "F# Bb C# E"},
				new Object[]{Chords.DOMINANT_SEVENTH, Note.G, "G B D F"},
				new Object[]{Chords.DOMINANT_SEVENTH, Note.A_FLAT, "Ab C Eb F#"},
				new Object[]{Chords.DOMINANT_SEVENTH, Note.A, "A C# E G"},
				new Object[]{Chords.DOMINANT_SEVENTH, Note.B_FLAT, "Bb D F Ab"},
				new Object[]{Chords.DOMINANT_SEVENTH, Note.B, "B Eb F# A"},
				new Object[]{Chords.MINOR_SEVENTH, Note.C, "C Eb G Bb"},
				new Object[]{Chords.MINOR_SEVENTH, Note.C_SHARP, "C# E Ab B"},
				new Object[]{Chords.MINOR_SEVENTH, Note.D, "D F A C"},
				new Object[]{Chords.MINOR_SEVENTH, Note.E_FLAT, "Eb F# Bb C#"},
				new Object[]{Chords.MINOR_SEVENTH, Note.E, "E G B D"},
				new Object[]{Chords.MINOR_SEVENTH, Note.F, "F Ab C Eb"},
				new Object[]{Chords.MINOR_SEVENTH, Note.F_SHARP, "F# A C# E"},
				new Object[]{Chords.MINOR_SEVENTH, Note.G, "G Bb D F"},
				new Object[]{Chords.MINOR_SEVENTH, Note.A_FLAT, "Ab B Eb F#"},
				new Object[]{Chords.MINOR_SEVENTH, Note.A, "A C E G"},
				new Object[]{Chords.MINOR_SEVENTH, Note.B_FLAT, "Bb C# F Ab"},
				new Object[]{Chords.MINOR_SEVENTH, Note.B, "B D F# A"},
		};
	}

	@Test
	public void majorTriadConsistsOfFirstThirdAndFifthNotesOfMajorScale() {
		//given
		Note root = randomNote();
		DiatonicScale majorScale = root.majorScale();

		//when
		Chord majorTriad = root.chord(Chords.MAJOR);

		//then
		assertThat(majorTriad.list()).containsOnly(majorScale.getFirst().note(), majorScale.getThird().note(), majorScale.getFifth().note());
	}

	@Test
	public void minorTriadConsistsOfFirstFlatThirdAndFifthNotesOfMajorScale() {
		//given
		Note root = randomNote();
		DiatonicScale majorScale = root.majorScale();

		//when
		Chord majorTriad = root.chord(Chords.MINOR);

		//then
		assertThat(majorTriad.list()).containsOnly(majorScale.getFirst().note(), majorScale.getThird().note().flat(), majorScale.getFifth().note());
	}

	@Test
	public void minorTriadDiffersFromMajorByFlatThird() {
		//given
		Note root = randomNote();

		//when
		TriadChord major = (TriadChord) root.chord(Chords.MAJOR);
		TriadChord minor = (TriadChord) root.chord(Chords.MINOR);

		//then
		assertThat(major.getFirst()).isEqualTo(minor.getFirst());
		assertThat(major.getFifth()).isEqualTo(minor.getFifth());
		assertThat(major.getThird().flat()).isEqualTo(minor.getThird());
	}

	@Test
	public void diminishedTriadConsistsOfFirstFlatThirdAndFlatFifthNotesOfMajorScale() {
		//given
		Note root = randomNote();
		DiatonicScale majorScale = root.majorScale();

		//when
		TriadChord diminished = (TriadChord) Chords.DIMINISHED.of(root);

		//then
		assertThat(majorScale.getFirst().note()).isEqualTo(diminished.getFirst());
		assertThat(majorScale.getThird().note().flat()).isEqualTo(diminished.getThird());
		assertThat(majorScale.getFifth().note().flat()).isEqualTo(diminished.getFifth());
	}

	@Test
	public void augmentedTriadConsistsOfFirstThirdAndSharpFifthNotesOfMajorScale() {
		//given
		Note root = randomNote();
		DiatonicScale majorScale = root.majorScale();

		//when
		TriadChord augmented = (TriadChord) Chords.AUGMENTED.of(root);

		//then
		assertThat(majorScale.getFirst().note()).isEqualTo(augmented.getFirst());
		assertThat(majorScale.getThird().note()).isEqualTo(augmented.getThird());
		assertThat(majorScale.getFifth().note().sharp()).isEqualTo(augmented.getFifth());
	}

	@Test
	public void sus2ChordConsistsOfFirstSecondAndFifthNotesOfMajorScale() {
		//given
		Note root = randomNote();
		DiatonicScale majorScale = root.majorScale();

		//when
		SuspendedChord sus2 = (SuspendedChord) root.chord(Chords.SUS2);

		//then
		assertThat(majorScale.getFirst().note()).isEqualTo(sus2.getFirst());
		assertThat(majorScale.getSecond().note()).isEqualTo(sus2.getSuspended());
		assertThat(majorScale.getFifth().note()).isEqualTo(sus2.getFifth());
	}

	@Test
	public void sus2SuspendsThirdNoteOfMajorScaleWithSecond() {
		//given
		Note root = randomNote();
		DiatonicScale majorScale = root.majorScale();

		//when
		SuspendedChord sus2 = (SuspendedChord) root.chord(Chords.SUS2);

		//then
		assertThat(sus2.list()).doesNotContain(majorScale.getThird().note());
		assertThat(sus2.getSuspended()).isEqualTo(majorScale.getSecond().note());
	}

	@Test
	public void sus4ChordConsistsOfFirstFourthAndFifthNotesOfMajorScale() {
		//given
		Note root = randomNote();
		DiatonicScale majorScale = root.majorScale();

		//when
		SuspendedChord sus4 = (SuspendedChord) root.chord(Chords.SUS4);

		//then
		assertThat(majorScale.getFirst().note()).isEqualTo(sus4.getFirst());
		assertThat(majorScale.getFourth().note()).isEqualTo(sus4.getSuspended());
		assertThat(majorScale.getFifth().note()).isEqualTo(sus4.getFifth());
	}

	@Test
	public void sus4SuspendsThirdNoteOfMajorScaleWithFourth() {
		//given
		Note root = randomNote();
		DiatonicScale majorScale = root.majorScale();

		//when
		SuspendedChord sus4 = (SuspendedChord) root.chord(Chords.SUS4);

		//then
		assertThat(sus4.list()).doesNotContain(majorScale.getThird().note());
		assertThat(sus4.getSuspended()).isEqualTo(majorScale.getFourth().note());
	}

	@Test
	public void majorSeventhChordConsistsOfFirstThirdFifthAndSeventhNotesOfMajorScale() {
		//given
		Note root = randomNote();
		DiatonicScale majorScale = root.majorScale();

		//when
		SeventhChord majorSeventh = (SeventhChord) root.chord(Chords.MAJOR_SEVENTH);

		//then
		assertThat(majorScale.getFirst().note()).isEqualTo(majorSeventh.getFirst());
		assertThat(majorScale.getThird().note()).isEqualTo(majorSeventh.getThird());
		assertThat(majorScale.getFifth().note()).isEqualTo(majorSeventh.getFifth());
		assertThat(majorScale.getSeventh().note()).isEqualTo(majorSeventh.getSeventh());
	}

	@Test
	public void minorSeventhChordConsistsOfFirstFlatThirdFifthAndFlatSeventhNotesOfMajorScale() {
		//given
		Note root = randomNote();
		DiatonicScale majorScale = root.majorScale();

		//when
		SeventhChord minorSeventh = (SeventhChord) root.chord(Chords.MINOR_SEVENTH);

		//then
		assertThat(majorScale.getFirst().note()).isEqualTo(minorSeventh.getFirst());
		assertThat(majorScale.getThird().note().flat()).isEqualTo(minorSeventh.getThird());
		assertThat(majorScale.getFifth().note()).isEqualTo(minorSeventh.getFifth());
		assertThat(majorScale.getSeventh().note().flat()).isEqualTo(minorSeventh.getSeventh());
	}

	@Test
	public void dominantSeventhChordConsistsOfFirstThirdFifthAndFlatSeventhNotesOfMajorScale() {
		//given
		Note root = randomNote();
		DiatonicScale majorScale = root.majorScale();

		//when
		SeventhChord dominantSeventh = (SeventhChord) root.chord(Chords.DOMINANT_SEVENTH);

		//then
		assertThat(majorScale.getFirst().note()).isEqualTo(dominantSeventh.getFirst());
		assertThat(majorScale.getThird().note()).isEqualTo(dominantSeventh.getThird());
		assertThat(majorScale.getFifth().note()).isEqualTo(dominantSeventh.getFifth());
		assertThat(majorScale.getSeventh().note().flat()).isEqualTo(dominantSeventh.getSeventh());
	}

	@Test
	public void chordsHaveNameShortNameAliasesAndReadableToString() {
		for (Note note : Note.values()) {
			for (Chords chordType : Chords.values()) {
				Chord chord = chordType.of(note);
				assertThat(chord.name()).isNotBlank();
				assertThat(chord.shortName()).isNotBlank();
				assertThat(chord.aliases()).isNotEmpty();
				assertThat(chord.toString()).contains(chord.list().stream().map(String::valueOf).collect(Collectors.toList()));
			}
		}
	}

	@Test
	@Parameters(method = "chordSizesParameters")
	public void chordSizes(Chords[] chordTypes, Note root, int expectedSize) {
		Arrays.stream(chordTypes).map(root::chord).forEach(chord -> assertThat(chord.size()).isEqualTo(expectedSize));
	}

	@SuppressWarnings("unused")
	Object[] chordSizesParameters() {
		return new Object[]{
				new Object[]{new Chords[]{Chords.MAJOR, Chords.MINOR, Chords.AUGMENTED, Chords.DIMINISHED, Chords.SUS2, Chords.SUS4}, randomNote(), 3},
				new Object[]{new Chords[]{Chords.MINOR_SEVENTH, Chords.MAJOR_SEVENTH, Chords.DOMINANT_SEVENTH}, randomNote(), 4}
		};
	}
}
