package org.taurus.scales.core;

import java.util.Random;

public final class TestUtils {

	private TestUtils() {
	}

	public static Note randomNote() {
		return Note.values()[new Random().nextInt(Note.values().length)];
	}
}
