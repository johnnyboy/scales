package org.taurus.scales.core;

import org.junit.Test;
import org.taurus.scales.core.chords.Chords;
import org.taurus.scales.core.scales.Scales;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

public class NoteTest {

	@Test
	public void twelveNotesInTotal() {
		assertThat(Note.values()).hasSize(12);
	}

	@Test
	public void sharpAndFlatAreBoundarySafe() {
		//given
		Note sharpened = Note.C;
		Note flattened = Note.C;

		//when
		for (int i = 0; i < Note.values().length * 2; i++) {
			sharpened = sharpened.sharp();
			flattened = flattened.flat();
		}

		//then
		assertThat(sharpened).isNotNull();
		assertThat(flattened).isNotNull();
	}

	@Test
	public void ofParsesCaseInsensitiveNames() {
		//given
		String[] names = Arrays.stream(Note.values()).map(n -> n.name).toArray(String[]::new);

		//when
		List<Note> notes = IntStream
				.range(0, names.length)
				.mapToObj(i -> (i % 2 != 0) ? names[i].toUpperCase() : names[i])
				.map(Note::of)
				.collect(Collectors.toList());

		//then
		assertThat(notes).hasSize(12);
		assertThat(notes).containsExactly(Note.values());
	}

	@Test(expected = IllegalArgumentException.class)
	public void ofThrowsIllegalArgumentExceptionOnNoMatchingNote() {
		//given
		String fakeNote = "E#";

		//when
		Note.of(fakeNote);

		//then
		//IllegalArgumentException
	}

	@Test
	public void allTypeOfChordsCanBeGeneratedThroughNote() {
		for (Note n : Note.values()) {
			for (Chords chordType : Chords.values()) {
				Chord chord = n.chord(chordType);
				assertThat(chord).isNotNull();
			}
		}
	}

	@Test
	public void allTypeOfScalesCanBeGeneratedThroughNote() {
		for (Note n : Note.values()) {
			for (Scales scaleType : Scales.values()) {
				Scale scale = n.scale(scaleType);
				assertThat(scale).isNotNull();
			}
		}
	}
}
