package org.taurus.scales.core.scales;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.taurus.scales.core.Note;
import org.taurus.scales.core.TestUtils;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.taurus.scales.core.TestUtils.randomNote;

public class ScalePatternTest {

	@Test(expected = IllegalArgumentException.class)
	public void shiftThrowsIllegalArgumentExceptionOnTooLargeIndex() {
		//given
		List<Note> original = randomNote().majorScale().list();
		int tooLargeIndex = original.size();

		//when
		ScalePattern.shift(original, tooLargeIndex);

		//then
	}

	@Test(expected = IllegalArgumentException.class)
	public void shiftThrowsIllegalArgumentExceptionOnNegativeIndex() {
		//given
		List<Note> original = randomNote().majorScale().list();
		int negativeIndex = -5;

		//when
		ScalePattern.shift(original, negativeIndex);

		//then
	}

	@Test
	public void shiftReturnsOriginalListOnZeroIndex() {
		//given
		List<Note> original = randomNote().majorScale().list();

		//when
		List<Note> shifted = ScalePattern.shift(original, 0);

		//then
		assertThat(shifted).isEqualTo(original);
	}

	@Test(expected = IllegalStateException.class)
	public void buildThrowsIllegalStateExceptionIfSizeDoesNotMatchExpected() {
		//given
		ScalePattern.Builder builder = ScalePattern.builder(randomNote(), 1);

		//when
		builder.tone().toneAndSemitone().build();

		//then
	}
}
