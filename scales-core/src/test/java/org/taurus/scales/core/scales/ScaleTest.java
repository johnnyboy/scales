package org.taurus.scales.core.scales;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.taurus.scales.core.Note;
import org.taurus.scales.core.Scale;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.taurus.scales.core.TestUtils.randomNote;

@RunWith(JUnitParamsRunner.class)
public class ScaleTest {

	@Test
	@Parameters(method = "scaleCorrectnessParameters")
	public void scaleCorrectness(Scales scaleType, Note root, String scale) {
		//given
		String[] expected = scale.replaceAll(" ", "").split(",");

		//when
		String[] actual = scaleType.of(root).list().stream().map(String::valueOf).toArray(String[]::new);

		//then
		assertThat(actual).isEqualTo(expected);
	}

	@SuppressWarnings("unused")
	Object[] scaleCorrectnessParameters() {
		return new Object[]{
				new Object[]{Scales.MAJOR, Note.C, "C, D, E, F, G, A, B, C"},
				new Object[]{Scales.MAJOR, Note.C_SHARP, "C#, Eb, F, F#, Ab, Bb, C, C#"},
				new Object[]{Scales.MAJOR, Note.D, "D, E, F#, G, A, B, C#, D"},
				new Object[]{Scales.MAJOR, Note.E_FLAT, "Eb, F, G, Ab, Bb, C, D, Eb"},
				new Object[]{Scales.MAJOR, Note.E, "E, F#, Ab, A, B, C#, Eb, E"},
				new Object[]{Scales.MAJOR, Note.F, "F, G, A, Bb, C, D, E, F"},
				new Object[]{Scales.MAJOR, Note.F_SHARP, "F#, Ab, Bb, B, C#, Eb, F, F#"},
				new Object[]{Scales.MAJOR, Note.G, "G, A, B, C, D, E, F#, G"},
				new Object[]{Scales.MAJOR, Note.A_FLAT, "Ab, Bb, C, C#, Eb, F, G, Ab"},
				new Object[]{Scales.MAJOR, Note.A, "A, B, C#, D, E, F#, Ab, A"},
				new Object[]{Scales.MAJOR, Note.B_FLAT, "Bb, C, D, Eb, F, G, A, Bb"},
				new Object[]{Scales.MAJOR, Note.B, "B, C#, Eb, E, F#, Ab, Bb, B"},
				new Object[]{Scales.HARMONIC_MINOR, Note.C, "C, D, Eb, F, G, Ab, B, C"},
				new Object[]{Scales.HARMONIC_MINOR, Note.C_SHARP, "C#, Eb, E, F#, Ab, A, C, C#"},
				new Object[]{Scales.HARMONIC_MINOR, Note.D, "D, E, F, G, A, Bb, C#, D"},
				new Object[]{Scales.HARMONIC_MINOR, Note.E_FLAT, "Eb, F, F#, Ab, Bb, B, D, Eb"},
				new Object[]{Scales.HARMONIC_MINOR, Note.E, "E, F#, G, A, B, C, Eb, E"},
				new Object[]{Scales.HARMONIC_MINOR, Note.F, "F, G, Ab, Bb, C, C#, E, F"},
				new Object[]{Scales.HARMONIC_MINOR, Note.F_SHARP, "F#, Ab, A, B, C#, D, F, F#"},
				new Object[]{Scales.HARMONIC_MINOR, Note.G, "G, A, Bb, C, D, Eb, F#, G"},
				new Object[]{Scales.HARMONIC_MINOR, Note.A_FLAT, "Ab, Bb, B, C#, Eb, E, G, Ab"},
				new Object[]{Scales.HARMONIC_MINOR, Note.A, "A, B, C, D, E, F, Ab, A"},
				new Object[]{Scales.HARMONIC_MINOR, Note.B_FLAT, "Bb, C, C#, Eb, F, F#, A, Bb"},
				new Object[]{Scales.HARMONIC_MINOR, Note.B, "B, C#, D, E, F#, G, Bb, B"},
				new Object[]{Scales.MINOR, Note.C, "C, D, Eb, F, G, Ab, Bb, C"},
				new Object[]{Scales.MINOR, Note.C_SHARP, "C#, Eb, E, F#, Ab, A, B, C#"},
				new Object[]{Scales.MINOR, Note.D, "D, E, F, G, A, Bb, C, D"},
				new Object[]{Scales.MINOR, Note.E_FLAT, "Eb, F, F#, Ab, Bb, B, C#, Eb"},
				new Object[]{Scales.MINOR, Note.E, "E, F#, G, A, B, C, D, E"},
				new Object[]{Scales.MINOR, Note.F, "F, G, Ab, Bb, C, C#, Eb, F"},
				new Object[]{Scales.MINOR, Note.F_SHARP, "F#, Ab, A, B, C#, D, E, F#"},
				new Object[]{Scales.MINOR, Note.G, "G, A, Bb, C, D, Eb, F, G"},
				new Object[]{Scales.MINOR, Note.A_FLAT, "Ab, Bb, B, C#, Eb, E, F#, Ab"},
				new Object[]{Scales.MINOR, Note.A, "A, B, C, D, E, F, G, A"},
				new Object[]{Scales.MINOR, Note.B_FLAT, "Bb, C, C#, Eb, F, F#, Ab, Bb"},
				new Object[]{Scales.MINOR, Note.B, "B, C#, D, E, F#, G, A, B"},
				new Object[]{Scales.MELODIC_MINOR, Note.C, "C, D, Eb, F, G, A, B, C"},
				new Object[]{Scales.MELODIC_MINOR, Note.C_SHARP, "C#, Eb, E, F#, Ab, Bb, C, C#"},
				new Object[]{Scales.MELODIC_MINOR, Note.D, "D, E, F, G, A, B, C#, D"},
				new Object[]{Scales.MELODIC_MINOR, Note.E_FLAT, "Eb, F, F#, Ab, Bb, C, D, Eb"},
				new Object[]{Scales.MELODIC_MINOR, Note.E, "E, F#, G, A, B, C#, Eb, E"},
				new Object[]{Scales.MELODIC_MINOR, Note.F, "F, G, Ab, Bb, C, D, E, F"},
				new Object[]{Scales.MELODIC_MINOR, Note.F_SHARP, "F#, Ab, A, B, C#, Eb, F, F#"},
				new Object[]{Scales.MELODIC_MINOR, Note.G, "G, A, Bb, C, D, E, F#, G"},
				new Object[]{Scales.MELODIC_MINOR, Note.A_FLAT, "Ab, Bb, B, C#, Eb, F, G, Ab"},
				new Object[]{Scales.MELODIC_MINOR, Note.A, "A, B, C, D, E, F#, Ab, A"},
				new Object[]{Scales.MELODIC_MINOR, Note.B_FLAT, "Bb, C, C#, Eb, F, G, A, Bb"},
				new Object[]{Scales.MELODIC_MINOR, Note.B, "B, C#, D, E, F#, Ab, Bb, B"},
				new Object[]{Scales.MAJOR_PENTATONIC, Note.C, "C, D, E, G, A"},
				new Object[]{Scales.MAJOR_PENTATONIC, Note.C_SHARP, "C#, Eb, F, Ab, Bb"},
				new Object[]{Scales.MAJOR_PENTATONIC, Note.D, "D, E, F#, A, B"},
				new Object[]{Scales.MAJOR_PENTATONIC, Note.E_FLAT, "Eb, F, G, Bb, C"},
				new Object[]{Scales.MAJOR_PENTATONIC, Note.E, "E, F#, Ab, B, C#"},
				new Object[]{Scales.MAJOR_PENTATONIC, Note.F, "F, G, A, C, D"},
				new Object[]{Scales.MAJOR_PENTATONIC, Note.F_SHARP, "F#, Ab, Bb, C#, Eb"},
				new Object[]{Scales.MAJOR_PENTATONIC, Note.G, "G, A, B, D, E"},
				new Object[]{Scales.MAJOR_PENTATONIC, Note.A_FLAT, "Ab, Bb, C, Eb, F"},
				new Object[]{Scales.MAJOR_PENTATONIC, Note.A, "A, B, C#, E, F#"},
				new Object[]{Scales.MAJOR_PENTATONIC, Note.B_FLAT, "Bb, C, D, F, G"},
				new Object[]{Scales.MAJOR_PENTATONIC, Note.B, "B, C#, Eb, F#, Ab"},
				new Object[]{Scales.MINOR_PENTATONIC, Note.C, "C, Eb, F, G, Bb"},
				new Object[]{Scales.MINOR_PENTATONIC, Note.C_SHARP, "C#, E, F#, Ab, B"},
				new Object[]{Scales.MINOR_PENTATONIC, Note.D, "D, F, G, A, C"},
				new Object[]{Scales.MINOR_PENTATONIC, Note.E_FLAT, "Eb, F#, Ab, Bb, C#"},
				new Object[]{Scales.MINOR_PENTATONIC, Note.E, "E, G, A, B, D"},
				new Object[]{Scales.MINOR_PENTATONIC, Note.F, "F, Ab, Bb, C, Eb"},
				new Object[]{Scales.MINOR_PENTATONIC, Note.F_SHARP, "F#, A, B, C#, E"},
				new Object[]{Scales.MINOR_PENTATONIC, Note.G, "G, Bb, C, D, F"},
				new Object[]{Scales.MINOR_PENTATONIC, Note.A_FLAT, "Ab, B, C#, Eb, F#"},
				new Object[]{Scales.MINOR_PENTATONIC, Note.A, "A, C, D, E, G"},
				new Object[]{Scales.MINOR_PENTATONIC, Note.B_FLAT, "Bb, C#, Eb, F, Ab"},
				new Object[]{Scales.MINOR_PENTATONIC, Note.B, "B, D, E, F#, A"},
				new Object[]{Scales.BLUES, Note.C, "C, Eb, F, F#, G, Bb"},
				new Object[]{Scales.BLUES, Note.C_SHARP, "C#, E, F#, G, Ab, B"},
				new Object[]{Scales.BLUES, Note.D, "D, F, G, Ab, A, C"},
				new Object[]{Scales.BLUES, Note.E_FLAT, "Eb, F#, Ab, A, Bb, C#"},
				new Object[]{Scales.BLUES, Note.E, "E, G, A, Bb, B, D"},
				new Object[]{Scales.BLUES, Note.F, "F, Ab, Bb, B, C, Eb"},
				new Object[]{Scales.BLUES, Note.F_SHARP, "F#, A, B, C, C#, E"},
				new Object[]{Scales.BLUES, Note.G, "G, Bb, C, C#, D, F"},
				new Object[]{Scales.BLUES, Note.A_FLAT, "Ab, B, C#, D, Eb, F#"},
				new Object[]{Scales.BLUES, Note.A, "A, C, D, Eb, E, G"},
				new Object[]{Scales.BLUES, Note.B_FLAT, "Bb, C#, Eb, E, F, Ab"},
				new Object[]{Scales.BLUES, Note.B, "B, D, E, F, F#, A"},
		};
	}

	@Test
	public void majorScaleRelativeMatchesNaturalMinorScaleOnSixthDegree() {
		Arrays.stream(Note.values()).map(Note::majorScale).collect(Collectors.toList()).forEach(majorScale -> {
			DiatonicScale relativeMinorScale = majorScale.relative();
			assertThat(relativeMinorScale.getFirst().note()).isEqualTo(majorScale.getSixth().note());
		});
	}

	@Test
	public void majorScaleParallelMatchesNaturalMinorScale() {
		Arrays.stream(Note.values()).map(Note::majorScale).forEach(majorScale -> {
			DiatonicScale parallelMinorScale = majorScale.parallel();
			Scale naturalMinorScale = majorScale.getFirst().note().scale(Scales.MINOR);
			assertThat(parallelMinorScale.list()).containsExactlyElementsOf(naturalMinorScale.list());
		});
	}

	@Test
	@Parameters(method = "scaleSizesParameters")
	public void scaleSizes(Scales[] scaleTypes, Note root, int expectedSize) {
		Arrays.stream(scaleTypes).map(root::scale).forEach(scale -> assertThat(scale.size()).isEqualTo(expectedSize));
	}

	@SuppressWarnings("unused")
	Object[] scaleSizesParameters() {
		return new Object[]{
				new Object[]{new Scales[]{Scales.MAJOR, Scales.MINOR, Scales.MELODIC_MINOR, Scales.HARMONIC_MINOR, Scales.DORIAN, Scales.PHRYGIAN, Scales.LYDIAN, Scales.MIXOLYDIAN, Scales.LOCRIAN}, randomNote(), 8},
				new Object[]{new Scales[]{Scales.MAJOR_PENTATONIC, Scales.MINOR_PENTATONIC}, randomNote(), 5},
				new Object[]{new Scales[]{Scales.BLUES}, randomNote(), 6}
		};
	}

	@Test
	public void modesHaveParallelAndRelativeScales() {
		for (Note note : Note.values()) {
			for (Scales scaleType : Arrays.asList(Scales.DORIAN, Scales.PHRYGIAN, Scales.LYDIAN, Scales.MIXOLYDIAN, Scales.LOCRIAN)) {
				Mode mode = (Mode) note.scale(scaleType);
				assertThat(mode.relative()).isNotNull();
				assertThat(mode.parallel()).isNotNull();
			}
		}
	}

	@Test
	public void scalesHaveNames() {
		for (Note note : Note.values()) {
			for (Scales scaleType : Scales.values()) {
				assertThat(note.scale(scaleType).name()).isNotBlank();
			}
		}
	}

	@Test
	public void scalesHaveReadableToString() {
		for (Note note : Note.values()) {
			for (Scales scaleType : Scales.values()) {
				Scale scale = note.scale(scaleType);
				assertThat(scale.toString()).contains(scale.name());
			}
		}
	}




	@Test
	public void diatonicAndPentatonicScalesHaveParallelRelativeScalesChordsAndChordProgressions() {
		for (Note note : Note.values()) {
			for (Scales scaleType : new Scales[]{Scales.MAJOR, Scales.MINOR, Scales.MELODIC_MINOR, Scales.HARMONIC_MINOR, Scales.DORIAN, Scales.PHRYGIAN, Scales.LYDIAN, Scales.MIXOLYDIAN, Scales.LOCRIAN}) {
				DiatonicScale diatonicScale = (DiatonicScale) note.scale(scaleType);
				assertThat(diatonicScale.parallel()).isNotNull();
				assertThat(diatonicScale.relative()).isNotNull();

			}
			for (Scales scaleType : new Scales[]{Scales.MAJOR_PENTATONIC, Scales.MINOR_PENTATONIC}) {
				PentatonicScale pentatonicScale = (PentatonicScale) scaleType.of(note);
				assertThat(pentatonicScale.parallel()).isNotNull();
				assertThat(pentatonicScale.relative()).isNotNull();
			}
		}
	}

	@Test
	public void diatonicAndPentatonicScalesHaveChordsAndChordProgressions() {
		for (Note note : Note.values()) {
			for (Scales scaleType : new Scales[]{Scales.MAJOR, Scales.MINOR, Scales.MELODIC_MINOR, Scales.HARMONIC_MINOR, Scales.DORIAN, Scales.PHRYGIAN, Scales.LYDIAN, Scales.MIXOLYDIAN, Scales.LOCRIAN}) {
				DiatonicScale diatonicScale = (DiatonicScale) note.scale(scaleType);
				assertThat(diatonicScale.chords()).isNotEmpty();
				assertThat(diatonicScale.chordProgressions()).isNotEmpty();
				diatonicScale.chordProgressions().forEach(this::assertChordProgression);

			}
			for (Scales scaleType : new Scales[]{Scales.MAJOR_PENTATONIC, Scales.MINOR_PENTATONIC}) {
				PentatonicScale pentatonicScale = (PentatonicScale) scaleType.of(note);
				assertThat(pentatonicScale.chords()).isNotEmpty();
				assertThat(pentatonicScale.chordProgressions()).isNotEmpty();
				pentatonicScale.chordProgressions().forEach(this::assertChordProgression);
			}
		}
	}

	private void assertChordProgression(ChordProgression chordProgression) {
		assertThat(chordProgression.chords()).isNotEmpty();
		assertThat(chordProgression.toString()).isNotBlank();
	}

	@Test
	public void diatonicAndPentatonicScalesHaveDegrees() {
		for (Note note : Note.values()) {
			for (Scales scaleType : new Scales[]{Scales.MAJOR, Scales.MINOR, Scales.MELODIC_MINOR, Scales.HARMONIC_MINOR, Scales.DORIAN, Scales.PHRYGIAN, Scales.LYDIAN, Scales.MIXOLYDIAN, Scales.LOCRIAN}) {
				DiatonicScale diatonicScale = (DiatonicScale) note.scale(scaleType);
				Stream.of(
						diatonicScale.getFirst(),
						diatonicScale.getSecond(),
						diatonicScale.getThird(),
						diatonicScale.getFourth(),
						diatonicScale.getFifth(),
						diatonicScale.getSixth(),
						diatonicScale.getSeventh(),
						diatonicScale.getEighth())
						.forEach(this::assertDegree);

			}
			for (Scales scaleType : new Scales[]{Scales.MAJOR_PENTATONIC, Scales.MINOR_PENTATONIC}) {
				PentatonicScale pentatonicScale = (PentatonicScale) scaleType.of(note);
				Stream.of(
						pentatonicScale.getFirst(),
						pentatonicScale.getSecond(),
						pentatonicScale.getThird(),
						pentatonicScale.getFourth(),
						pentatonicScale.getFifth())
						.forEach(this::assertDegree);
			}
		}
	}

	private void assertDegree(Degree degree) {
		assertThat(degree).isNotNull();
		assertThat(degree.chord()).isNotNull();
		assertThat(degree.note()).isNotNull();
		assertThat(degree.toString()).isNotBlank();
	}
}
