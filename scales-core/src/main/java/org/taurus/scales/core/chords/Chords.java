package org.taurus.scales.core.chords;

import org.taurus.scales.core.Chord;
import org.taurus.scales.core.Note;
import org.taurus.scales.core.scales.DiatonicScale;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum Chords {
	MAJOR {
		@Override
		public TriadChord of(Note note) {
			DiatonicScale majorScale = note.majorScale();
			List<Note> list = Arrays.asList(
					majorScale.getFirst().note(),
					majorScale.getThird().note(),
					majorScale.getFifth().note()
			);
			return new Triad(list, "Major chord", Arrays.asList("", "M", "maj", "major"));
		}
	}, MINOR {
		@Override
		public TriadChord of(Note note) {
			DiatonicScale majorScale = note.majorScale();
			List<Note> list = Arrays.asList(
					majorScale.getFirst().note(),
					majorScale.getThird().note().flat(),
					majorScale.getFifth().note()
			);
			return new Triad(list, "Minor chord", Arrays.asList("m", "min", "minor"));
		}
	},
	DIMINISHED {
		@Override
		public TriadChord of(Note note) {
			DiatonicScale majorScale = note.majorScale();
			List<Note> list = Arrays.asList(
					majorScale.getFirst().note(),
					majorScale.getThird().note().flat(),
					majorScale.getFifth().note().flat()
			);
			return new Triad(list, "Diminished chord", Collections.singletonList("dim"));
		}
	},
	AUGMENTED {
		@Override
		public TriadChord of(Note note) {
			DiatonicScale majorScale = note.majorScale();
			List<Note> list = Arrays.asList(
					majorScale.getFirst().note(),
					majorScale.getThird().note(),
					majorScale.getFifth().note().sharp()
			);
			return new Triad(list, "Augmented chord", Collections.singletonList("aug"));
		}
	},
	SUS2 {
		@Override
		public SuspendedChord of(Note note) {
			DiatonicScale majorScale = note.majorScale();
			List<Note> list = Arrays.asList(
					majorScale.getFirst().note(),
					majorScale.getSecond().note(),
					majorScale.getFifth().note()
			);
			return new Suspended(list, "Suspended second chord", Collections.singletonList("sus2"));
		}
	}, SUS4 {
		@Override
		public SuspendedChord of(Note note) {
			DiatonicScale majorScale = note.majorScale();
			List<Note> list = Arrays.asList(
					majorScale.getFirst().note(),
					majorScale.getFourth().note(),
					majorScale.getFifth().note()
			);
			return new Suspended(list, "Suspended fourth chord", Collections.singletonList("sus4"));
		}
	},
	MAJOR_SEVENTH {
		@Override
		public SeventhChord of(Note note) {
			DiatonicScale majorScale = note.majorScale();
			List<Note> list = Arrays.asList(
					majorScale.getFirst().note(),
					majorScale.getThird().note(),
					majorScale.getFifth().note(),
					majorScale.getSeventh().note());
			return new Seventh(list, "Major seventh chord", Arrays.asList("M7", "maj7", "major 7"));
		}
	}, MINOR_SEVENTH {
		@Override
		public SeventhChord of(Note note) {
			DiatonicScale majorScale = note.majorScale();
			List<Note> list = Arrays.asList(
					majorScale.getFirst().note(),
					majorScale.getThird().note().flat(),
					majorScale.getFifth().note(),
					majorScale.getSeventh().note().flat()
			);
			return new Seventh(list, "Minor seventh chord", Arrays.asList("m7", "min7", "minor 7"));
		}
	},
	DOMINANT_SEVENTH {
		@Override
		public SeventhChord of(Note note) {
			DiatonicScale majorScale = note.majorScale();
			List<Note> list = Arrays.asList(
					majorScale.getFirst().note(),
					majorScale.getThird().note(),
					majorScale.getFifth().note(),
					majorScale.getSeventh().note().flat()
			);
			return new Seventh(list, "Dominant seventh chord", Arrays.asList("7", "dom7", "dominant seventh"));
		}
	};

	public abstract Chord of(Note note);
}
