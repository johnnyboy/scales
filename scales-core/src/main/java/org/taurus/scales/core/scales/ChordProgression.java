package org.taurus.scales.core.scales;

import org.taurus.scales.core.Chord;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ChordProgression {
	private final Collection<Chord> chords;

	public ChordProgression(Degree... degrees) {
		this.chords = Collections.unmodifiableCollection(
				Stream.of(degrees).map(Degree::chord).collect(Collectors.toList())
		);
	}

	public Collection<Chord> chords() {
		return chords;
	}

	@Override
	public String toString() {
		return chords().toString();
	}
}
