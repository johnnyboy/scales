package org.taurus.scales.core.chords;

import org.taurus.scales.core.Note;

import java.util.Collections;
import java.util.List;

class Suspended implements SuspendedChord {

	private final List<Note> list;
	private final String name;
	private final List<String> aliases;

	Suspended(List<Note> list, String name, List<String> aliases) {
		this.list = Collections.unmodifiableList(list);
		this.name = name;
		this.aliases = Collections.unmodifiableList(aliases);
	}

	@Override
	public Note getFirst() {
		return list().get(0);
	}

	@Override
	public Note getSuspended() {
		return list().get(1);
	}

	@Override
	public Note getFifth() {
		return list().get(2);
	}

	@Override
	public List<String> aliases() {
		return aliases;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public List<Note> list() {
		return list;
	}

	@Override
	public int size() {
		return list().size();
	}

	@Override
	public String shortName() {
		return String.format("%s%s", getFirst(), aliases().get(0));
	}

	@Override
	public String toString() {
		return String.format("{%s} %s:%s", shortName(), name(), list());
	}
}
