package org.taurus.scales.core.chords;

import org.taurus.scales.core.Chord;
import org.taurus.scales.core.Note;

public interface SeventhChord extends Chord {

	Note getFirst();

	Note getThird();

	Note getFifth();

	Note getSeventh();
}
