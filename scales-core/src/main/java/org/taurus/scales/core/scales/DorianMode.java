package org.taurus.scales.core.scales;

import org.taurus.scales.core.Note;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

class DorianMode implements Mode {
	private final Note parent;
	private final List<Note> list;
	private final String name;

	DorianMode(Note parent) {
		this.parent = parent;
		this.name = "Dorian mode";
		this.list = ScalePattern.shift(parent.majorScale().list(), 1);
	}

	@Override
	public Note parent() {
		return parent;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public List<Note> list() {
		return list;
	}

	@Override
	public Degree getFirst() {
		return Degree.minor(list.get(0));
	}

	@Override
	public Degree getSecond() {
		return Degree.minor(list.get(1));
	}

	@Override
	public Degree getThird() {
		return Degree.major(list.get(2));
	}

	@Override
	public Degree getFourth() {
		return Degree.major(list.get(3));
	}

	@Override
	public Degree getFifth() {
		return Degree.minor(list.get(4));
	}

	@Override
	public Degree getSixth() {
		return Degree.diminished(list.get(5));
	}

	@Override
	public Degree getSeventh() {
		return Degree.major(list.get(6));
	}

	@Override
	public Degree getEighth() {
		return Degree.minor(list.get(7));
	}

	@Override
	public Collection<ChordProgression> chordProgressions() {
		return Collections.unmodifiableCollection(Arrays.asList(
				new ChordProgression(getFirst(), getFourth(), getFirst(), getSeventh(), getFirst()),
				new ChordProgression(getFirst(), getSeventh(), getFirst(), getSeventh(), getFirst()),
				new ChordProgression(getFirst(), getFourth(), getSecond(), getFifth(), getSeventh(), getFirst()),
				new ChordProgression(getFirst(), getThird(), getFourth(), getFirst(), getSeventh(), getFirst())
		));
	}
}
