package org.taurus.scales.core;

import org.taurus.scales.core.chords.Chords;
import org.taurus.scales.core.scales.DiatonicScale;
import org.taurus.scales.core.scales.Scales;

public enum Note {
	C("C"), C_SHARP("C#"), D("D"), E_FLAT("Eb"), E("E"), F("F"), F_SHARP("F#"), G(
			"G"), A_FLAT("Ab"), A("A"), B_FLAT("Bb"), B("B");

	public final String name;

	Note(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	public Note sharp() {
		int index = ordinal();
		int size = Note.values().length;
		if (index == size - 1) {
			return Note.values()[0];
		}
		return Note.values()[index + 1];
	}

	public Note flat() {
		int index = ordinal();
		if (index == 0) {
			return Note.values()[Note.values().length - 1];
		}
		return Note.values()[index - 1];
	}

	public Scale scale(Scales type){
		return type.of(this);
	}

	public DiatonicScale majorScale() {
		return (DiatonicScale) scale(Scales.MAJOR);
	}

	public Chord chord(Chords type) {
		return type.of(this);
	}

	public static Note of(String name) {
		for (Note note : values()) {
			if (note.name.equalsIgnoreCase(name)) {
				return note;
			}
		}
		throw new IllegalArgumentException(String.format(
				"No matching note for %s", name));
	}
}
