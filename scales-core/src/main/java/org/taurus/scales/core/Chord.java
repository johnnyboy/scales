package org.taurus.scales.core;

import java.util.List;

public interface Chord {

	List<String> aliases();

	String name();

	List<Note> list();

	int size();

	String shortName();
}
