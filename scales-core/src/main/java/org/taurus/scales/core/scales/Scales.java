package org.taurus.scales.core.scales;

import org.taurus.scales.core.Note;
import org.taurus.scales.core.Scale;
import org.taurus.scales.core.scales.PrintableScales.PrintableDiatonicScale;
import org.taurus.scales.core.scales.PrintableScales.PrintableHexatonicScale;
import org.taurus.scales.core.scales.PrintableScales.PrintableMode;

public enum Scales {
	MAJOR {
		@Override
		public DiatonicScale of(Note note) {
			return new PrintableDiatonicScale(new MajorScale(note));
		}
	}, MINOR {
		@Override
		public DiatonicScale of(Note note) {
			return new PrintableDiatonicScale(new MinorScale(note));
		}
	}, MELODIC_MINOR {
		@Override
		public DiatonicScale of(Note note) {
			return new PrintableDiatonicScale(new MelodicMinorScale(note));
		}
	}, HARMONIC_MINOR {
		@Override
		public DiatonicScale of(Note note) {
			return new PrintableDiatonicScale(new HarmonicMinorScale(note));
		}
	},
	DORIAN {
		@Override
		public Mode of(Note note) {
			return new PrintableMode(new DorianMode(note));
		}
	},
	PHRYGIAN {
		@Override
		public Mode of(Note note) {
			return new PrintableMode(new PhrygianMode(note));
		}
	},
	LYDIAN {
		@Override
		public Mode of(Note note) {
			return new PrintableMode(new LydianMode(note));
		}
	},
	MIXOLYDIAN {
		@Override
		public Mode of(Note note) {
			return new PrintableMode(new MixolydianMode(note));
		}
	},
	LOCRIAN {
		@Override
		public Mode of(Note note) {
			return new PrintableMode(new LocrianMode(note));
		}
	},
	MAJOR_PENTATONIC {
		@Override
		public PentatonicScale of(Note note) {
			return new PrintableScales.PrintablePentatonicScale(new MajorPentatonicScale(note));
		}
	}, MINOR_PENTATONIC {
		@Override
		public PentatonicScale of(Note note) {
			return new PrintableScales.PrintablePentatonicScale(new MinorPentatonicScale(note));
		}
	}, BLUES {
		@Override
		public HexatonicScale of(Note note) {
			return new PrintableHexatonicScale(new BluesHexatonicScale(note));
		}
	};

	public abstract Scale of(Note note);

	public static final int DIATONIC_SCALE_SIZE = 8;

}