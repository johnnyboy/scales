package org.taurus.scales.core.scales;

import org.taurus.scales.core.Chord;
import org.taurus.scales.core.Note;
import org.taurus.scales.core.chords.Chords;

public class Degree {

	private final Note note;
	private final Chords chordType;

	private Degree(Note note, Chords chordType) {
		this.note = note;
		this.chordType = chordType;
	}

	static Degree major(Note note) {
		return new Degree(note, Chords.MAJOR);
	}

	static Degree minor(Note note) {
		return new Degree(note, Chords.MINOR);
	}

	static Degree diminished(Note note) {
		return new Degree(note, Chords.DIMINISHED);
	}

	static Degree augmented(Note note) {
		return new Degree(note, Chords.AUGMENTED);
	}

	public Chord chord() {
		return chordType.of(note);
	}

	public Note note() {
		return note;
	}
}