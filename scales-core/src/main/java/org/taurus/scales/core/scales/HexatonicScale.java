package org.taurus.scales.core.scales;

import org.taurus.scales.core.Note;
import org.taurus.scales.core.Scale;

import java.util.List;

public interface HexatonicScale extends Scale{

	Note getFirst();

	Note getSecond();

	Note getThird();

	Note getFourth();

	Note getFifth();

	Note getSixth();
}
