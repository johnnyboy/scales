package org.taurus.scales.core.scales;

import org.taurus.scales.core.Note;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class ScalePattern {
	private final List<Note> list;

	private ScalePattern(List<Note> list) {
		this.list = list;
	}

	List<Note> list() {
		return list;
	}

	static Builder builder(Note root, int size) {
		return new Builder(root, size);
	}

	static List<Note> shift(List<Note> original, int index) {
		if (index < 0 || index >= original.size()) {
			throw new IllegalArgumentException(String.format("Index %d is out of list range [0..%d]", index, original.size() - 1));
		}
		if (index == 0) {
			return original;
		}
		List<Note> shifted = new ArrayList<>(original.subList(0, original.size() - 1));
		Collections.rotate(shifted, -index);
		shifted.add(shifted.get(0));
		return shifted;
	}

	static class Builder {
		private Note current;
		private final List<Note> list;
		private final int size;

		Builder(Note root, int size) {
			this.size = size;
			current = root;
			list = new ArrayList<>(size);
			list.add(root);
		}

		Builder tone() {
			current = current.sharp().sharp();
			list.add(current);
			return this;
		}

		Builder semitone() {
			current = current.sharp();
			list.add(current);
			return this;
		}

		Builder toneAndSemitone() {
			current = current.sharp().sharp().sharp();
			list.add(current);
			return this;
		}

		ScalePattern build() {
			if (this.list.size() != size) {
				throw new IllegalStateException(String.format("List size %d does not match expected size %d", this.list.size(), size));
			}
			return new ScalePattern(list);
		}
	}
}
