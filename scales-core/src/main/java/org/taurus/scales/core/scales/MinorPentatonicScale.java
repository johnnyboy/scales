package org.taurus.scales.core.scales;

import org.taurus.scales.core.Note;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class MinorPentatonicScale implements PentatonicScale {
	private final List<Note> list;
	private final String name;
	private final DiatonicScale minorScale;

	MinorPentatonicScale(Note root) {
		this.name = "Minor pentatonic scale";
		this.minorScale = (DiatonicScale) root.scale(Scales.MINOR);
		this.list = Collections.unmodifiableList(
				Stream.of(
						minorScale.getFirst(),
						minorScale.getThird(),
						minorScale.getFourth(),
						minorScale.getFifth(),
						minorScale.getSeventh())
						.map(Degree::note)
						.collect(Collectors.toList())
		);
	}

	@Override
	public List<Note> list() {
		return list;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public PentatonicScale relative() {
		return (PentatonicScale) getSecond().note().scale(Scales.MAJOR_PENTATONIC);
	}

	@Override
	public PentatonicScale parallel() {
		return (PentatonicScale) getFirst().note().scale(Scales.MAJOR_PENTATONIC);
	}

	@Override
	public Degree getFirst() {
		return minorScale.getFirst();
	}

	@Override
	public Degree getSecond() {
		return minorScale.getThird();
	}

	@Override
	public Degree getThird() {
		return minorScale.getFourth();
	}

	@Override
	public Degree getFourth() {
		return minorScale.getFifth();
	}

	@Override
	public Degree getFifth() {
		return minorScale.getSeventh();
	}

	@Override
	public Collection<ChordProgression> chordProgressions() {
		return minorScale.chordProgressions();
	}
}
