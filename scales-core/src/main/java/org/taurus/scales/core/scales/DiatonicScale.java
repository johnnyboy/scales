package org.taurus.scales.core.scales;

import org.taurus.scales.core.Chord;
import org.taurus.scales.core.Note;
import org.taurus.scales.core.Scale;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface DiatonicScale extends Scale {

	String name();

	List<Note> list();

	/**
	 * @return note - root also called tonic
	 */
	Degree getFirst();

	/**
	 * @return note - supertonic
	 */
	Degree getSecond();

	/**
	 * @return note - mediant
	 */
	Degree getThird();

	/**
	 * @return note - subdominant
	 */
	Degree getFourth();

	/**
	 * @return note - dominant
	 */
	Degree getFifth();

	/**
	 * @return note - submediant
	 */
	Degree getSixth();

	/**
	 * @return note - subtonic
	 */
	Degree getSeventh();

	/**
	 * @return note - octave
	 */
	Degree getEighth();

	default List<Chord> chords() {
		return Collections.unmodifiableList(
				Stream.of(getFirst(), getSecond(), getThird(), getFourth(), getFifth(), getSixth(), getSeventh(), getEighth())
						.map(Degree::chord)
						.collect(Collectors.toList())
		);
	}

	DiatonicScale relative();

	DiatonicScale parallel();

	Collection<ChordProgression> chordProgressions();
}
