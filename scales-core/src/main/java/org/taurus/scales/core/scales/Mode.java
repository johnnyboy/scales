package org.taurus.scales.core.scales;

import org.taurus.scales.core.Note;

public interface Mode extends DiatonicScale {

	Note parent();

	@Override
	default DiatonicScale relative() {
		return parent().majorScale();
	}

	@Override
	default DiatonicScale parallel() {
		return parent().majorScale().parallel();
	}
}
