package org.taurus.scales.core.scales;

import org.taurus.scales.core.Note;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

class PhrygianMode implements Mode {
	private final Note parent;
	private final List<Note> list;
	private final String name;

	PhrygianMode(Note parent) {
		this.parent = parent;
		this.name = "Phrygian mode";
		this.list = ScalePattern.shift(parent.majorScale().list(), 2);
	}

	@Override
	public Note parent() {
		return parent;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public List<Note> list() {
		return list;
	}

	@Override
	public Degree getFirst() {
		return Degree.minor(list.get(0));
	}

	@Override
	public Degree getSecond() {
		return Degree.major(list.get(1));
	}

	@Override
	public Degree getThird() {
		return Degree.major(list.get(2));
	}

	@Override
	public Degree getFourth() {
		return Degree.minor(list.get(3));
	}

	@Override
	public Degree getFifth() {
		return Degree.diminished(list.get(4));
	}

	@Override
	public Degree getSixth() {
		return Degree.major(list.get(5));
	}

	@Override
	public Degree getSeventh() {
		return Degree.minor(list.get(6));
	}

	@Override
	public Degree getEighth() {
		return Degree.minor(list.get(7));
	}

	@Override
	public Collection<ChordProgression> chordProgressions() {
		return Collections.unmodifiableCollection(Arrays.asList(
				new ChordProgression(getFirst(), getSecond(), getThird(), getFirst()),
				new ChordProgression(getFirst(), getFifth(), getFirst(), getSecond(), getFirst()),
				new ChordProgression(getFirst(), getFourth(), getFirst(), getSecond(), getFirst()),
				new ChordProgression(getFirst(), getThird(), getSeventh(), getFirst())
		));
	}
}
