package org.taurus.scales.core.chords;

import org.taurus.scales.core.Note;

import java.util.Collections;
import java.util.List;

class Seventh implements SeventhChord {

	private final List<Note> list;
	private final String name;
	private final List<String> aliases;

	Seventh(List<Note> list, String name, List<String> aliases) {
		this.list = Collections.unmodifiableList(list);
		this.name = name;
		this.aliases = Collections.unmodifiableList(aliases);
	}

	@Override
	public Note getFirst() {
		return list().get(0);
	}

	@Override
	public Note getThird() {
		return list().get(1);
	}

	@Override
	public Note getFifth() {
		return list().get(2);
	}

	@Override
	public Note getSeventh() {
		return list().get(3);
	}

	@Override
	public int size() {
		return list().size();
	}

	@Override
	public List<Note> list() {
		return list;
	}

	@Override
	public List<String> aliases() {
		return aliases;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public String shortName() {
		return String.format("%s%s", getFirst(), aliases().get(0));
	}

	@Override
	public String toString() {
		return String.format("{%s} %s:%s", shortName(), name(), list());
	}
}
