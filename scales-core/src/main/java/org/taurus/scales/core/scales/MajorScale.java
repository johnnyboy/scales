package org.taurus.scales.core.scales;

import org.taurus.scales.core.Note;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;


class MajorScale implements DiatonicScale {
	private final List<Note> list;
	private final String name;

	MajorScale(Note root) {
		this.name = "Major scale (Ionian mode)";
		this.list = ScalePattern.builder(root, Scales.DIATONIC_SCALE_SIZE)
				.tone()
				.tone()
				.semitone()
				.tone()
				.tone()
				.tone()
				.semitone()
				.build()
				.list();
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public List<Note> list() {
		return list;
	}

	@Override
	public Degree getFirst() {
		return Degree.major(list.get(0));
	}

	@Override
	public Degree getSecond() {
		return Degree.minor(list.get(1));
	}

	@Override
	public Degree getThird() {
		return Degree.minor(list.get(2));
	}

	@Override
	public Degree getFourth() {
		return Degree.major(list.get(3));
	}

	@Override
	public Degree getFifth() {
		return Degree.major(list.get(4));
	}

	@Override
	public Degree getSixth() {
		return Degree.minor(list.get(5));
	}

	@Override
	public Degree getSeventh() {
		return Degree.diminished(list.get(6));
	}

	@Override
	public Degree getEighth() {
		return Degree.major(list.get(7));
	}

	@Override
	public DiatonicScale relative() {
		return (DiatonicScale) getSixth().note().scale(Scales.MINOR);
	}

	@Override
	public DiatonicScale parallel() {
		return (DiatonicScale) getFirst().note().scale(Scales.MINOR);
	}

	@Override
	public Collection<ChordProgression> chordProgressions() {
		return Collections.unmodifiableCollection(Arrays.asList(
				new ChordProgression(getFirst(), getFourth(), getSixth(), getFifth(), getFirst()),
				new ChordProgression(getFirst(), getSecond(), getFourth(), getFifth(), getFirst()),
				new ChordProgression(getFirst(), getFifth(), getSixth(), getFourth(), getFirst()),
				new ChordProgression(getFirst(), getThird(), getSixth(), getFifth(), getFirst())
		));
	}
}
