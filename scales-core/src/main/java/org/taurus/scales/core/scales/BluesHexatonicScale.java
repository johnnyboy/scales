package org.taurus.scales.core.scales;

import org.taurus.scales.core.Note;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class BluesHexatonicScale implements HexatonicScale {
	private final List<Note> list;
	private final String name;

	BluesHexatonicScale(Note root) {
		DiatonicScale majorScale = root.majorScale();
		this.name = "Blues hexatonic scale";
		this.list = Collections.unmodifiableList(Arrays.asList(
				majorScale.getFirst().note(),
				majorScale.getThird().note().flat(),
				majorScale.getFourth().note(),
				majorScale.getFifth().note().flat(),
				majorScale.getFifth().note(),
				majorScale.getSeventh().note().flat()

		));
	}

	@Override
	public List<Note> list() {
		return list;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public Note getFirst() {
		return list.get(0);
	}

	@Override
	public Note getSecond() {
		return list.get(1);
	}

	@Override
	public Note getThird() {
		return list.get(2);
	}

	@Override
	public Note getFourth() {
		return list.get(3);
	}

	@Override
	public Note getFifth() {
		return list.get(4);
	}

	@Override
	public Note getSixth() {
		return list.get(5);
	}
}
