package org.taurus.scales.core.scales;

import org.taurus.scales.core.Note;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

class MelodicMinorScale implements DiatonicScale {

	private final List<Note> list;
	private final String name;

	MelodicMinorScale(Note root) {
		this.name = "Melodic minor";
		this.list = ScalePattern.builder(root, Scales.DIATONIC_SCALE_SIZE)
				.tone()
				.semitone()
				.tone()
				.tone()
				.tone()
				.tone()
				.semitone()
				.build()
				.list();
	}

	@Override
	public DiatonicScale relative() {
		return getThird().note().majorScale();
	}

	@Override
	public DiatonicScale parallel() {
		return getFirst().note().majorScale();
	}

	@Override
	public List<Note> list() {
		return list;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public Degree getFirst() {
		return Degree.minor(list.get(0));
	}

	@Override
	public Degree getSecond() {
		return Degree.minor(list.get(1));
	}

	@Override
	public Degree getThird() {
		return Degree.augmented(list.get(2));
	}

	@Override
	public Degree getFourth() {
		return Degree.major(list.get(3));
	}

	@Override
	public Degree getFifth() {
		return Degree.major(list.get(4));
	}

	@Override
	public Degree getSixth() {
		return Degree.diminished(list.get(5));
	}

	@Override
	public Degree getSeventh() {
		return Degree.diminished(list.get(6));
	}

	@Override
	public Degree getEighth() {
		return Degree.minor(list.get(7));
	}

	@Override
	public Collection<ChordProgression> chordProgressions() {
		return Collections.singleton(new ChordProgression(getFirst(), getFourth(), getFifth(), getFirst()));
	}
}
