package org.taurus.scales.core.scales;

import org.taurus.scales.core.Note;

import java.util.Collection;
import java.util.List;

class PrintableScales {
	private PrintableScales(){}

	static class PrintableDiatonicScale implements DiatonicScale {
		private final DiatonicScale scale;

		PrintableDiatonicScale(DiatonicScale scale) {
			this.scale = scale;
		}

		@Override
		public String name() {
			return scale.name();
		}

		@Override
		public List<Note> list() {
			return scale.list();
		}

		@Override
		public Degree getFirst() {
			return scale.getFirst();
		}

		@Override
		public Degree getSecond() {
			return scale.getSecond();
		}

		@Override
		public Degree getThird() {
			return scale.getThird();
		}

		@Override
		public Degree getFourth() {
			return scale.getFourth();
		}

		@Override
		public Degree getFifth() {
			return scale.getFifth();
		}

		@Override
		public Degree getSixth() {
			return scale.getSixth();
		}

		@Override
		public Degree getSeventh() {
			return scale.getSeventh();
		}

		@Override
		public Degree getEighth() {
			return scale.getEighth();
		}

		@Override
		public DiatonicScale relative() {
			return scale.relative();
		}

		@Override
		public DiatonicScale parallel() {
			return scale.parallel();
		}

		@Override
		public Collection<ChordProgression> chordProgressions() {
			return scale.chordProgressions();
		}

		@Override
		public String toString() {
			return String.format("{%s} %s:\t%s", getFirst().note(), name(), list());
		}
	}

	static class PrintableMode implements Mode {
		private final Mode mode;

		PrintableMode(Mode mode) {
			this.mode = mode;
		}

		@Override
		public Note parent() {
			return mode.parent();
		}

		@Override
		public String name() {
			return mode.name();
		}

		@Override
		public List<Note> list() {
			return mode.list();
		}

		@Override
		public Degree getFirst() {
			return mode.getFirst();
		}

		@Override
		public Degree getSecond() {
			return mode.getSecond();
		}

		@Override
		public Degree getThird() {
			return mode.getThird();
		}

		@Override
		public Degree getFourth() {
			return mode.getFourth();
		}

		@Override
		public Degree getFifth() {
			return mode.getFifth();
		}

		@Override
		public Degree getSixth() {
			return mode.getSixth();
		}

		@Override
		public Degree getSeventh() {
			return mode.getSeventh();
		}

		@Override
		public Degree getEighth() {
			return mode.getEighth();
		}

		@Override
		public Collection<ChordProgression> chordProgressions() {
			return mode.chordProgressions();
		}

		@Override
		public String toString() {
			return String.format("{%s} %s over {%s}:\t%s", getFirst().note(), name(), parent(), list());
		}
	}

	static class PrintableHexatonicScale implements HexatonicScale {
		private final HexatonicScale scale;

		PrintableHexatonicScale(HexatonicScale scale) {
			this.scale = scale;
		}

		@Override
		public List<Note> list() {
			return scale.list();
		}

		@Override
		public String name() {
			return scale.name();
		}

		@Override
		public Note getFirst() {
			return scale.getFirst();
		}

		@Override
		public Note getSecond() {
			return scale.getSecond();
		}

		@Override
		public Note getThird() {
			return scale.getThird();
		}

		@Override
		public Note getFourth() {
			return scale.getFourth();
		}

		@Override
		public Note getFifth() {
			return scale.getFifth();
		}

		@Override
		public Note getSixth() {
			return scale.getSixth();
		}

		@Override
		public String toString() {
			return String.format("{%s} %s:\t%s", getFirst(), name(), list());
		}
	}

	static class PrintablePentatonicScale implements PentatonicScale {
		private final PentatonicScale scale;

		PrintablePentatonicScale(PentatonicScale scale) {
			this.scale = scale;
		}

		@Override
		public List<Note> list() {
			return scale.list();
		}

		@Override
		public String name() {
			return scale.name();
		}

		@Override
		public PentatonicScale relative() {
			return scale.relative();
		}

		@Override
		public PentatonicScale parallel() {
			return scale.parallel();
		}

		@Override
		public Degree getFirst() {
			return scale.getFirst();
		}

		@Override
		public Degree getSecond() {
			return scale.getSecond();
		}

		@Override
		public Degree getThird() {
			return scale.getThird();
		}

		@Override
		public Degree getFourth() {
			return scale.getFourth();
		}

		@Override
		public Degree getFifth() {
			return scale.getFifth();
		}

		@Override
		public Collection<ChordProgression> chordProgressions() {
			return scale.chordProgressions();
		}

		@Override
		public String toString() {
			return String.format("{%s} %s:\t%s", getFirst().note(), name(), list());
		}
	}

}
