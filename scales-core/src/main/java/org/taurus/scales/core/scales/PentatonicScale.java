package org.taurus.scales.core.scales;

import org.taurus.scales.core.Chord;
import org.taurus.scales.core.Note;
import org.taurus.scales.core.Scale;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface PentatonicScale extends Scale {

	List<Note> list();

	String name();

	PentatonicScale relative();

	PentatonicScale parallel();

	Degree getFirst();

	Degree getSecond();

	Degree getThird();

	Degree getFourth();

	Degree getFifth();

	default List<Chord> chords() {
		return Collections.unmodifiableList(
				Stream.of(getFirst(), getSecond(), getThird(), getFourth(), getFifth())
						.map(Degree::chord)
						.collect(Collectors.toList())
		);
	}

	Collection<ChordProgression> chordProgressions();
}
