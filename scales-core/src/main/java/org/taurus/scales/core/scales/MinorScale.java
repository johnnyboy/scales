package org.taurus.scales.core.scales;

import org.taurus.scales.core.Note;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

class MinorScale implements DiatonicScale {
	private final List<Note> list;
	private final String name;

	MinorScale(Note root) {
		this.name = "Minor scale (Aeolian mode)";
		this.list = ScalePattern.builder(root, Scales.DIATONIC_SCALE_SIZE)
				.tone()
				.semitone()
				.tone()
				.tone()
				.semitone()
				.tone()
				.tone()
				.build()
				.list();
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public List<Note> list() {
		return list;
	}

	@Override
	public Degree getFirst() {
		return Degree.minor(list.get(0));
	}

	@Override
	public Degree getSecond() {
		return Degree.diminished(list.get(1));
	}

	@Override
	public Degree getThird() {
		return Degree.major(list.get(2));
	}

	@Override
	public Degree getFourth() {
		return Degree.minor(list.get(3));
	}

	@Override
	public Degree getFifth() {
		return Degree.minor(list.get(4));
	}

	@Override
	public Degree getSixth() {
		return Degree.major(list.get(5));
	}

	@Override
	public Degree getSeventh() {
		return Degree.major(list.get(6));
	}

	@Override
	public Degree getEighth() {
		return Degree.minor(list.get(7));
	}

	@Override
	public DiatonicScale relative() {
		return getThird().note().majorScale();
	}

	@Override
	public DiatonicScale parallel() {
		return getFirst().note().majorScale();
	}

	@Override
	public Collection<ChordProgression> chordProgressions() {
		return Collections.unmodifiableCollection(Arrays.asList(
				new ChordProgression(getFirst(), getSixth(), getSeventh(), getFirst()),
				new ChordProgression(getFirst(), getFourth(), getFifth(), getFirst()),
				new ChordProgression(getFirst(), getThird(), getFirst(), getSeventh(), getFirst())
		));
	}
}
