package org.taurus.scales.core.scales;

import org.taurus.scales.core.Note;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class MajorPentatonicScale implements PentatonicScale {
	private final List<Note> list;
	private final String name;
	private final DiatonicScale majorScale;

	MajorPentatonicScale(Note root) {
		this.name = "Major pentatonic scale";
		this.majorScale = root.majorScale();
		this.list = Collections.unmodifiableList(
				Stream.of(
						majorScale.getFirst(),
						majorScale.getSecond(),
						majorScale.getThird(),
						majorScale.getFifth(),
						majorScale.getSixth())
						.map(Degree::note)
						.collect(Collectors.toList())
		);
	}

	@Override
	public List<Note> list() {
		return list;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public PentatonicScale relative() {
		return (PentatonicScale) getFifth().note().scale(Scales.MINOR_PENTATONIC);
	}

	@Override
	public PentatonicScale parallel() {
		return (PentatonicScale) getFirst().note().scale(Scales.MINOR_PENTATONIC);
	}

	@Override
	public Degree getFirst() {
		return majorScale.getFirst();
	}

	@Override
	public Degree getSecond() {
		return majorScale.getSecond();
	}

	@Override
	public Degree getThird() {
		return majorScale.getThird();
	}

	@Override
	public Degree getFourth() {
		return majorScale.getFifth();
	}

	@Override
	public Degree getFifth() {
		return majorScale.getSixth();
	}

	@Override
	public Collection<ChordProgression> chordProgressions() {
		return majorScale.chordProgressions();
	}
}
