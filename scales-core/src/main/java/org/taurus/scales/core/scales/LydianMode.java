package org.taurus.scales.core.scales;

import org.taurus.scales.core.Note;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

class LydianMode implements Mode {
	private final Note parent;
	private final List<Note> list;
	private final String name;

	LydianMode(Note parent) {
		this.parent = parent;
		this.name = "Lydian mode";
		this.list = ScalePattern.shift(parent.majorScale().list(), 3);
	}

	@Override
	public Note parent() {
		return parent;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public List<Note> list() {
		return list;
	}

	@Override
	public Degree getFirst() {
		return Degree.major(list.get(0));
	}

	@Override
	public Degree getSecond() {
		return Degree.major(list.get(1));
	}

	@Override
	public Degree getThird() {
		return Degree.minor(list.get(2));
	}

	@Override
	public Degree getFourth() {
		return Degree.diminished(list.get(3));
	}

	@Override
	public Degree getFifth() {
		return Degree.major(list.get(4));
	}

	@Override
	public Degree getSixth() {
		return Degree.minor(list.get(5));
	}

	@Override
	public Degree getSeventh() {
		return Degree.minor(list.get(6));
	}

	@Override
	public Degree getEighth() {
		return Degree.major(list.get(7));
	}

	@Override
	public Collection<ChordProgression> chordProgressions() {
		return Collections.unmodifiableCollection(Arrays.asList(
				new ChordProgression(getFirst(), getSecond(), getFirst(), getSecond()),
				new ChordProgression(getFirst(), getSecond(), getFirst(), getSecond(), getFourth(), getFifth(), getFourth(), getFifth(), getFirst()),
				new ChordProgression(getFirst(), getSixth(), getSecond(), getFifth(), getFirst()),
				new ChordProgression(getFirst(), getFifth(), getFirst(), getSecond(), getFirst())
		));
	}
}
