package org.taurus.scales.core.chords;

import org.taurus.scales.core.Chord;
import org.taurus.scales.core.Note;

public interface SuspendedChord extends Chord {

	Note getFirst();

	Note getSuspended();

	Note getFifth();
}
