package org.taurus.scales.core;

import java.util.List;

public interface Scale {

	String name();

	List<Note> list();

	default int size() {
		return list().size();
	}
}
